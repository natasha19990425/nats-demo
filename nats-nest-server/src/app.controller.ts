import { Controller, Get } from '@nestjs/common';
import { Ctx, MessagePattern, NatsContext, Payload } from '@nestjs/microservices';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly minionAppService: AppService) {}

  @MessagePattern('pub.*')
  public getDate(@Payload() data, @Ctx() context: NatsContext) {
    console.log(`Subject: ${context.getSubject()}`);
    console.log('payload', data)
    return data;
  }

  @MessagePattern("sum")
  public sum(data: number[]): number {

    console.log(`2.Server  ${new Date().toISOString()} --> 接收需求`);
    const result = this.minionAppService.sum(data);
    console.log(`4.Server  ${new Date().toISOString()} --> 發布結果: sum result`, result); 
    return result;
  }

  @MessagePattern({ cmd: 'reverse' })
  public reverse(message: string): string {
    console.log('MinionAppController: reverse', message);
    return this.minionAppService.reverse(message);
  }
}