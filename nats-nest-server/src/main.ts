import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.connectMicroservice( {
    transport: Transport.NATS,
    options: {
      url: 'nats://localhost:4222',
    },
  });


  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);
  const port = process.env.port || 3011;

  await app.startAllMicroservicesAsync();
  await app.listen(port, () => {
    console.log('Listening at http://localhost:' + port + '/' + globalPrefix);
    console.log(`1.Server  ${new Date().toISOString()} --> NATS Connect`);
  });
}
bootstrap();
